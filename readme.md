# Simple Physics for JME

This is designed to be an alternative to the (excellent) complex physics built into JME, but for when your needs are simpler.

To see it in action, run HelloSimplePhysics or watch this video: https://www.youtube.com/watch?v=hLJgE911qXE

Licence is MIT.
 

## FEATURES:
* Configurable parameters for entities: Bounciness, Air resistance, gravity.
* A character control for first-person movement, including jumping and walking up steps.
* Can apply forces to entities, both one-off and constant.
* Kinematic entities, including automatic position correction due to overlaps
* Full collision detection between all entities.
* Entities will bounce off each other (unless bounciness set to 0).
* Can configure it that certain entities should not collide with others (e.g. prevent friendly fire).
* Runs in the main JME thread.
* Less CPU intensive.


## NOT FEATURES YET:
* No rotational force.
* No "pushing" mechanism.


## CREDITS
Written by Stephen Carlyle-Smith (http://twitter.com/stephencsmith, stephen.carlylesmith@googlemail.com)